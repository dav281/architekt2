$(function () {
    $('.image-order').change(function (e) {
        console.log('change');
        var form = $(this).closest('form')

        $.nette.ajax({
            type: 'POST',
            url: form.attr('action'),
            data: form.serialize(),
        });
    });
});