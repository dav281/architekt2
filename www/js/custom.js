$(function(){
    var $sortableList = $(".sortable");

    var sortEventHandler = function(event, ui){
       var items = ($('.sortable').sortable('toArray'));
       console.log(items);
       $.ajax({
           type: "POST",
           url: $(".sortable").data("link"),
           data: {"items": items}
        });
    };

    $sortableList.sortable({
        stop: sortEventHandler
    });

    $('.open-modal').click(function (e) {
        e.preventDefault();
        var titlePhotoId = $(this).data('modal-id');
        $('#modal-wrapper-'+titlePhotoId).show();
    });

    $('.close-modal').click(function (e) {
        e.preventDefault();
        var titlePhotoId = $(this).data('modal-id');
        $('#modal-wrapper-'+titlePhotoId).hide();
    });

});