<?php

/** @var \Nette\DI\Container $container */
$container = require __DIR__ . '/../app/bootstrap.php';

/*if( \Tracy\Debugger::$productionMode ){
	throw new \Nette\Application\ForbiddenRequestException("No Authorization Exception");
}

/** @var \Doctrine\DBAL\Connection $conn */
$conn = $container->getByType(\Doctrine\DBAL\Connection::class);
$dbal = new \Nextras\Migrations\Bridges\DoctrineDbal\DoctrineAdapter($conn);
$driver = new Nextras\Migrations\Drivers\MySqlDriver($dbal);

$controllerClass = 'Nextras\\Migrations\\Controllers\\' . (PHP_SAPI === 'cli' ? 'Console' : 'Http') . 'Controller';
/** @var Nextras\Migrations\Controllers\ConsoleController|Nextras\Migrations\Controllers\HttpController $controller */
$controller = new $controllerClass($driver);
$controller->addGroup('structures', __DIR__ . '/../migrations/structures');
$controller->addGroup('basic-data', __DIR__ . '/../migrations/basic-data', ['structures']);
//$controller->addGroup('dummy-data', __DIR__ . '/../migrations/dummy-data', ['basic-data']);
$controller->addGroup('dummy-data', __DIR__ . '/../migrations/dummy-data', ['basic-data']);
$controller->addExtension('sql', new Nextras\Migrations\Extensions\SqlHandler($driver));
$controller->run();