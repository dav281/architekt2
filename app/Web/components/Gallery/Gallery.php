<?php

namespace App\Components;

use App\Model\ImageManager;
use Nette;

class Gallery extends Nette\Application\UI\Control
{

    /** @var \App\Model\ImageManager */
    private $imageManager;


    public function setImageManager(ImageManager $imageManager)
    {
        $this->imageManager = $imageManager;
    }

    public function render($data = [], $text = "")
    {
        $template = $this->getPresenter()->getTemplate();
        $template->setFile(__DIR__ . "/templates/gallery.latte");

        $template->sizes = $this->getImageSizes();
        $template->data = $data;
        $template->text = $text;

        $template->render();
    }

    public function getImageSizes()
    {
        $params = $this->getPresenter()->context->getParameters();
        $sizes = $params["responsive"]["display"];

        return $this->imageManager->getImageSizes($sizes);
    }

}
interface GalleryFactory
{
    /**
     * @return Gallery
     */
    public function create();
}
