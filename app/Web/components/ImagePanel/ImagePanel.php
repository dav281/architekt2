<?php

namespace App\Components;


use App\Model\ImageManager;
use Nette\Application\UI\Control;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Tracy\Debugger;

class ImagePanel extends Control
{

    /** @var \App\Model\ImageManager */
    private $imageManager;

    /** @var \Nette\Http\SessionSection */
    public $data;

    /** @var SessionSection */
    public $orderBy;


    public function __construct(Session $session)
	{
		parent::__construct();
		$this->orderBy = $session->getSection('imageOrderBy')->order;
	}

	public function setImageManager(ImageManager $imageManager)
    {
        $this->imageManager = $imageManager;
    }

    public function setSession(Session $session)
    {
        $this->data = $session->getSection("data");
    }

    public function render()
    {
        $template = $this->getPresenter()->getTemplate();
        $template->setFile(__DIR__ . "/templates/default.latte");

        $template->image_info = $this->imageManager->getImageInfo();
        $images = $this->imageManager->getImagesForRender($this->orderBy);
        $sizes = $this->getImageSizes();
        $template->data = $this->data->text;

        $template->images = $images;
        $template->sizes = $sizes;

        $template->render();
    }


    public function getImageSizes()
    {
        $params = $this->getPresenter()->context->getParameters();
        $sizes = $params["responsive"]["display"];

        return $this->imageManager->getImageSizes($sizes);
    }
}

interface ImagePanelFactory
{
    /**
     * @return ImagePanel
     */
    public function create();
}