<?php

namespace App\Web\Forms;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

interface IImagePanelOrderFactory
{
	/**
	 * @return ImagePanelOrder
	 */
	function create();
}

class ImagePanelOrder extends Control
{

	/** @var SessionSection */
	private $orderBy;

	public function __construct(Session $session)
	{
		parent::__construct();
		$this->orderBy = $session->getSection('imageOrderBy');
	}

	public function render()
	{
		$this->getTemplate()->setFile(__DIR__ . "/ImagePanelOrder.latte");
		$this->getTemplate()->render();
	}

	public function createComponentForm()
	{
		$form = new Form();
		$form->addRadioList('orderBy', 'Řadit podle', [0 => "Datum realizace", 1 => "Datum přidání"])
			->setDefaultValue(0)
			->setRequired(TRUE);
		$form->addSubmit('submit')
			->setAttribute('class', 'ajax');

		$form->onSuccess[] = [$this, 'formProcess'];

		return $form;
	}

	public function formProcess(Form $form, ArrayHash $values)
	{
		if($values['orderBy'] === 0){
			$order = ['year' => 'DESC'];
		}else{
			$order = ['id' => 'DESC'];
		}
		$this->orderBy->order = $order;
		if($this->getPresenter()->isAjax()){
			$this->getPresenter()->payload->order = TRUE;
			$this->getPresenter()->redrawControl('imagePanel');
		}
	}

}