<?php

namespace App\Web\Presenters;

use App\Components\GalleryFactory;
use App\Components\ImagePanelFactory;
use App\Model\ImageManager;
use Nette;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @persistent */
    public $locale;

    /** @var \Kdyby\Translation\Translator @inject */
    public $translator;

    /** @var ImagePanelFactory @inject */
    public $imagePanelFactory;

    /** @var GalleryFactory @inject */
    public $galleryFactory;

    /** @var ImageManager @inject */
    public $imageManager;

    /** @var \Nette\Http\Session */
    public $session;

    /** @var \Nette\Http\SessionSection */
    public $data;

    public function __construct(Nette\Http\Session $session)
    {
        parent::__construct();
        $this->session = $session;
        $this->data = $session->getSection("data");
    }

    public function createComponentImagePanel()
    {
        $form = $this->imagePanelFactory->create();
        $form->setImageManager($this->imageManager);
        $form->setSession($this->session);
        return $form;
    }

    public function createComponentGallery()
    {
        $component = $this->galleryFactory->create();
        $component->setImageManager($this->imageManager);
        return $component;
    }
}
