<?php

namespace App\Web\Presenters;

use App\Admin\Model\Entities\ArchitectManager;
use App\Admin\Model\Entities\TitlePhoto;
use App\Model\ImageManager;
use App\Web\Forms\IImagePanelOrderFactory;
use App\Web\Forms\ImagePanelOrder;
use Doctrine\ORM\EntityManager;
use Nette\Http\Session;
use Tracy\Debugger;


class HomepagePresenter extends BasePresenter
{

    public $architects;

    public $titlePhotoRepository;

    /** @var IImagePanelOrderFactory @inject*/
    public $imagePanelOrderFactory;

    public function __construct(Session $session, ArchitectManager $architectManager, EntityManager $entityManager)
    {
        parent::__construct($session);
        $this->architects = $architectManager;
        $this->titlePhotoRepository = $entityManager->getRepository(TitlePhoto::class);
    }

    public function renderDefault()
    {
        if ($this->isAjax()) {
            $this->getTemplate()->isAjax = true;
        }else{
            $this->getTemplate()->isAjax = false;
        }

        $this->getTemplate()->titlePhotos = $this->titlePhotoRepository->findBy([], ["ordered" => "ASC"]);
        $this->getTemplate()->architects = $this->architects->getArchitects();
    }

    public function handleClick($category_id)
    {
        if ($this->isAjax()) {
            $this->getTemplate()->data = $this->imageManager->getGalleryImages($category_id);
            $this->getTemplate()->text = $this->imageManager->getGalleryInfo($category_id);
            $this->redrawControl();
        }
    }

    public function createComponentImagePanelOrder()
	{
		return $this->imagePanelOrderFactory->create();
	}

}
