<?php

namespace App\Model;

use App\Admin\Model\Entities\Image;
use App\Admin\Model\Entities\Image_category;
use Doctrine\ORM\EntityManager;
use Nette;
use Nette\Object;
use Tracy\Debugger;

/**
 * Class ImageManager
 * @package App\Model
 */
class ImageManager extends Object
{

    /** @var EntityManager */
    private $em;

    /**
     * ImageManager constructor.
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager)
    {
        $this->em = $manager;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getImageRepository()
    {
        return $this->em->getRepository(Image::getClassName());
    }

    /**
     * @param array $criteria criteria like [id = 5]
     * @param array|null $order_by
     * @param int|null $limit
     * @return array
     */
    public function findImage(array $criteria, array $order_by = null, $limit = null)
    {
        $query = $this->em->getRepository(Image::getClassName());
        $images = $query->findBy($criteria, $order_by, $limit);
        return $images;
    }

    /**
     * @param int|null $limit => How much results do you need to get
     * @return array
     */
    public function getNewImages($limit = null)
    {
        $order_by = ["uploaded" => "DESC"];
        $criteria = [];
        return $this->findImage($criteria, $order_by, $limit);
    }


	/**
	 * function will return images prepared for render with [main => 1]
	 * @param array $orderBy
	 * @return array
	 */
    public function getImagesForRender($orderBy = [])
    {
		return $this->findImage(["main" => 1], $orderBy);
    }

    /**
     * function will return parameters from config about image_size to min_weight
     * @return mixed
     * @throws NoSizesDefinedException
     */
    public function getImageSizes($sizes)
    {
        if ($sizes == null || $sizes == [] || !isset($sizes)) {
            throw new NoSizesDefinedException("You have to set image_width = min-width in config.neon");
        }

        return $sizes;
    }

    /**
     * Return formatted array for latte
     * @return array
     */
    public function getImageInfo()
    {
        $info = $this->em->getRepository(Image_category::getClassName())->findAll();

        $image = [];
        $iterator = 0;

        foreach ($info as $data) {
            $image[$data->id] = $info[$iterator];
            $iterator++;
        }

        return $image;
    }

    public function changeMainImage($id)
    {
        $mainImage = $this->em->getRepository(Image::getClassName())->findOneBy(["id" => $id]);
        $category_id = $mainImage->category_id;
        $oldMainImage = $this->em->getRepository(Image::getClassName())->findOneBy(["main" => 1, "category_id" => $category_id]);

        $oldMainImage->main = 0;
        $mainImage->main = 1;

        $this->em->persist($oldMainImage);
        $this->em->persist($mainImage);
        $this->em->flush();
    }

    public function deleteImage($id)
    {
        $dir = "www/photos/";

        $repository = $this->em->getRepository(Image::getClassName());
        $main = $repository->findOneBy(["id" => $id]);

        $category_id = $main->category_id;
        $category_name = $main->name;
        $category_main = $main->main;

        $wholeImages = $repository->findBy(["name" => $category_name]);
        foreach ($wholeImages as $wholeImage) {
            $this->em->remove($wholeImage);
        }

        foreach (Nette\Utils\Finder::findFiles("$category_name" . "*.jpg")->in($dir) as $key => $file) {
            Nette\Utils\FileSystem::delete($key);
        }

        $this->em->flush();

        $category_images = $repository->findBy(["category_id" => $category_id]);
        if ($category_images == [] || $category_images == null || empty($category_images)) {
            //Delete whole category
            $categoryDB = $this->em->getRepository(Image_category::getClassName());
            $cat_image = $categoryDB->findOneBy(["id" => $category_id]);
            $this->em->remove($cat_image);
            $this->em->flush();
            return;
        }

        if ($category_main == 1) {
            $image = $repository->findOneBy(["category_id" => $category_id, "main" => 0]);
            $image->main = 1;
            $this->em->persist($image);
            $this->em->flush();
        }
    }

    public function deleteReference($category_id)
    {
        $dir = "www/photos/";

        $deletedImages = [];
        $name = "";

        $category = $this->em->getRepository(Image_category::getClassName())->findOneBy(["id" => $category_id]);
        $category_images = $this->em->getRepository(Image::getClassName())->findBy(["category_id" => $category_id]);

        foreach ($category_images as $category_image) {
            if ($name != $category_image->name) {
                $deletedImages[] = $category_image->name;
            }
            $name = $category_image->name;
            $this->em->remove($category_image);
        }
        foreach ($deletedImages as $deletedImage) {
            foreach (Nette\Utils\Finder::findFiles("$deletedImage" . "*.jpg")->in($dir) as $key => $file) {
                Nette\Utils\FileSystem::delete($key);
            }
        }

        $this->em->remove($category);
        $this->em->flush();
    }

    public function getGalleryImages($category_id)
    {
        $category_images = $this->em->getRepository(Image::getClassName())->findBy(["category_id" => $category_id]);

        $images = [];
        $name = "";

        foreach ($category_images as $image) {
            if ($image->main == 1) {
                $images[] = $image;
            }
        }

        foreach ($category_images as $image) {
            if ($image->main != 1 and $image->name != $name) {
                $images[] = $image;
            }
            $name = $image->name;
        }

        return $images;
    }

    public function getGalleryInfo($category_id)
    {
        $category = $this->em->getRepository(Image_category::getClassName())->findOneBy(["id" => $category_id]);
        return $category;
    }

    /**
     * Get all images linked with category
     * @param int $id
     * @return array
     * @throws BadArgumentException
     */

    public function getLinkedImages($id)
    {
        $images = $this->em->getRepository(Image::getClassName())->findBy(["category_id" => $id]);
        $name = "";
        $return = [];
        foreach ($images as $image) {
            if ($name != $image->name) {
                $return[] = $image;
            }
            $name = $image->name;
        }
        return $return;
    }


}

class BadArgumentException extends \Exception
{

}

class NoSizesDefinedException extends \Exception
{

}