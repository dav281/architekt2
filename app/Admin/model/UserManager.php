<?php

namespace App\Admin\Model;

use Kdyby\Doctrine\EntityManager;
use App\Admin\Model\Entities\User;
use Nette;

class UserManager extends Nette\Object
{
    public $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function add($user, $password, $email = null, $role = "Guest")
    {

        $userDao = new User();
        $pass = Nette\Security\Passwords::hash($password);

        $userDao->setUser($user);
        $userDao->setPassword($pass);
        if ($email != null) $userDao->setEmail($email);
        if ($role != "Guest") $userDao->setRole($role);

        $this->em->persist($userDao);

        $this->em->flush();
    }

    public function addCustomer($values)
    {

        $userDao = new User();
        $pass = Nette\Security\Passwords::hash($values->password);

        $userDao->setUser($values->name);
        $userDao->setPassword($pass);
        $userDao->setEmail($values->email);
        $userDao->setRole("administrator");

        $this->em->persist($userDao);

        $this->em->flush();
    }

    public function checkEmailExists($email)
    {
        if ($this->em->getRepository(User::getClassName())->findOneBy(["email" => $email])) {
            return true;
        } else {
            return false;
        }
    }

    public function getCredentials($email)
    {

        return $userDao = $this->em->getRepository(User::getClassName())->findOneBy(["email" => $email]);

    }


}
