<?php

namespace App\Admin\Model\Entities;


use Doctrine\ORM\EntityManager;
use Kdyby\Doctrine\Entities\BaseEntity;

class ArchitectManager extends BaseEntity
{

    public $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getArchitects()
    {
        $architects = $this->em->getRepository(Architect::getClassName())->findAll();
        return $architects;
    }

    public function getArchitect($id)
    {
        return $this->em->getRepository(Architect::getClassName())->findOneBy(["id" => $id]);
    }

    public function deleteArchitect($architect)
    {
        $this->em->remove($architect);
        $this->em->flush();
    }


}