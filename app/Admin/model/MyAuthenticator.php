<?php

namespace App\Admin\Model;

use Nette\Security as NS;
use Nette;


class MyAuthenticator extends Nette\Object implements NS\IAuthenticator
{
    public $um;

    function __construct(UserManager $userManager)
    {
        $this->um = $userManager;
    }

    function authenticate(array $credentials)
    {
        list($email, $password) = $credentials;
        $row = $this->um->getCredentials($email);

        if (!$row) {
            throw new NS\AuthenticationException('User not found.');
        }

        if (!NS\Passwords::verify($password, $row->password)) {
            throw new NS\AuthenticationException('Invalid password.');
        }

        return new NS\Identity($row->id, $row->role, array('username' => $row->user));
    }
}