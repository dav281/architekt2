<?php

namespace App\Admin\Model;


use App\Admin\Model\Entities\Image;
use Kdyby\Doctrine\EntityManager;
use Nette;

Class ImageManager extends Nette\Object
{

    private $em;
    private $im;

    public function __construct(EntityManager $entityManager, \App\Model\ImageManager $manager)
    {
        $this->em = $entityManager;
        $this->im = $manager;
    }

    public function getAllImages()
    {
        $image = $this->em->getRepository(Image::getClassName());
        $image->findBy(["main" => 1]);
        return $image->findAll();
    }

    public function getImage($id)
    {
        $image = $this->em->getRepository(Image::getClassName());
        $image->findBy(["id" => $id]);
        return $image;
    }

    public function getImages($category_id)
    {
        $images = $this->em->getRepository(Image::getClassName());
        $images->findBy(["category_id" => $category_id]);
        return $images;

    }

}