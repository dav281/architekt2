<?php

namespace App\Admin\Forms;

use App\Admin\Model\Entities\TitlePhoto;
use Doctrine\ORM\EntityManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

interface ITitlePhotoDescriptionFormFactory
{
	/**
	 * @param TitlePhoto $titlePhoto
	 * @return TitlePhotoDescriptionForm
	 */
	function create(TitlePhoto $titlePhoto);
}

class TitlePhotoDescriptionForm extends Control
{

	/** @var TitlePhoto */
	private $titlePhoto;

	/** @var EntityManager */
	private $entityManager;

	public function __construct(TitlePhoto $titlePhoto, EntityManager $entityManager)
	{
		parent::__construct();
		$this->titlePhoto = $titlePhoto;
		$this->entityManager = $entityManager;
	}

	public function render()
	{
		$this->getTemplate()->setFile(__DIR__ . "/TitlePhotoDescriptionForm.latte");
		$this->getTemplate()->titlePhotoId = $this->titlePhoto->getId();
		$this->getTemplate()->titlePhoto = $this->titlePhoto;
		$this->getTemplate()->render();
	}

	public function createComponentForm()
	{
		$form = new Form();
		$form->addText('title');
		$form->addHidden('titlePhotoid', $this->titlePhoto->getId());
		$form->addSubmit('submit');

		$form->onSuccess[] = [$this, 'formProcess'];

		return $form;
	}

	public function formProcess(Form $form, ArrayHash $values)
	{
		$this->titlePhoto->setTitle($values['title']);
		$this->entityManager->persist($this->titlePhoto);
		$this->entityManager->flush();
	}

}