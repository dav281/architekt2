<?php

namespace App\Admin\Forms;

use App\Admin\Model\Entities\TitlePhoto;
use Doctrine\ORM\EntityManager;
use Nette\Application\UI\Form;
use Nette\Http\FileUpload;
use Nette\Object;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nette\Utils\Image;
use Tracy\Debugger;


interface ITitlePhotoFormFactory
{
	/**
	 * @return TitlePhotoForm
	 */
	function create(EntityManager $entityManager);
}

class TitlePhotoForm extends Object
{
	/** @var EntityManager */
	private $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

	public function createComponentForm()
	{
		$form = new Form();

		$form->addMultiUpload('titlePhotos', 'Titulní fotky');
		$form->addSubmit('upload');

		$form->onValidate[] = [$this, 'formValidate'];
		$form->onSuccess[] = [$this, 'formSuccess'];

		return $form;
	}

	public function formValidate(Form $form, ArrayHash $values)
	{
		/** @var FileUpload $file */
		foreach ($values->titlePhotos as $file)
		{
			if($file->isImage()){
				$image = Image::fromFile($file);
				if($image->getWidth() <= $image->getHeight()){
					$form->addError("Obrázek ". $file->getName() . " Musí být na šířku");
				}
			}
		}
	}

	public function formSuccess(Form $form, ArrayHash $values)
	{
		$titlePhotos = $values->titlePhotos;
		/** @var FileUpload $file */
		foreach ($titlePhotos as $file) {
			if($file->isImage()) {
				$extension = pathinfo($file->getName(), PATHINFO_EXTENSION);
				$titlePhoto = new TitlePhoto();
				$titlePhoto->setName(md5(uniqid($file->name . $file->getTemporaryFile() . time())) . "." . $extension);
				$titlePhoto->setOrdered(0);
				$titlePhoto->setCreatedAt(new DateTime());

				$this->entityManager->persist($titlePhoto);

				$image = Image::fromFile($file);
				$image->resize(1920, 1080, IMAGE::SHRINK_ONLY);
				$image->save("www/titlePhotos/" . $titlePhoto->getName(), 80);
			}
		}

		$this->entityManager->flush();
	}
}