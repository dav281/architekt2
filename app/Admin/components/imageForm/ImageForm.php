<?php

namespace App\Admin\Components;

use App\Admin\Model\Entities\Image;
use App\Admin\Model\Entities\Image_category;
use App\Model\NoSizesDefinedException;
use Doctrine\ORM\EntityManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Http\Session;
use Nette\Utils\DateTime;
use Nette\Utils\Strings;
use Tracy\Debugger;


class ImageForm extends Control
{
    /** @var Image_category @inject */
    public $image_category;

    /** @var Image @inject */
    public $image;

    /** @var EntityManager @inject */
    public $em;

    /** @var \Nette\Http\Session */
    private $session;

    /** @var \Nette\Http\SessionSection */
    private $sessionData;

    public function setSession(Session $session)
    {
        $this->session = $session;
        $this->sessionData = $this->session->getSection("sessionData");
    }

    public function render()
    {
        $this->template->setFile(__DIR__ . "/templates/default.latte");

        $this->sessionData->update = false;

        $this->template->render();
    }

    public function renderImage()
    {
        $this->template->setFile(__DIR__ . "/templates/image.latte");

        $this->template->render();
    }

    public function renderUpdate($category_id)
    {
        $this->template->setFile(__DIR__ . "/templates/default.latte");

        $img = $this->em->getRepository(Image::getClassName())->findOneBy(["category_id" => $category_id]);

        $category = $this->em->getRepository(Image_category::getClassName())->findOneBy(["id" => $category_id]);

        $this->sessionData->ImageCategory = $category;
        $this->sessionData->ImageYear = $img->year;

        $this->sessionData->update = true;

        $this->template->render();
    }

    public function setEntityManager(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function createComponentHeaderImagesForm()
    {
        $form = new Form();

        $form->addMultiUpload("image", "Obrázek")
            ->addRule(Form::IMAGE, "Lze vložit pouze obrázky")
            ->setRequired();

        $form->addSubmit("subm", "nahrát na server");

        $form->onSubmit[] = [$this, "imageFormProcess"];

        return $form;
    }

    public function imageFormProcess(Form $form)
    {
        $values = $form->getValues();


        foreach ($values->image as $temp) {
        	$imgName = md5("$temp->name" . time());
            $img = \Nette\Utils\Image::fromFile($temp);

			$img->resize(NULL, 644, \Nette\Utils\Image::SHRINK_ONLY);
			$img->save("renderImages/" . $imgName .".jpg", 75);
        }
    }

    public function createComponentHeaderForm()
    {
        $form = new Form();
        $form->addText("cat_name", "Název projektu")
            ->setRequired();

        $form->addText("description", "Krátký popisek")
            ->setRequired();

        $form->addText("location", "Lokace");

        $form->addText("year", "Rok realizace projektu")
            ->setRequired();

        $form->addTextArea("text", "Text k projektu", 70, 15)
            ->setRequired();

        $form->addMultiUpload("image", "Obrázek")
            ->addRule(Form::IMAGE, "Lze vložit pouze obrázky")
            ->setRequired(!$this->sessionData->update);

        $form->addSubmit("submit", "Nahrát na server");

        $form->onSubmit[] = [$this, "headerFormProcess"];

        if ($this->sessionData->update) {
            $category = $this->sessionData->ImageCategory;

            $year = $this->sessionData->ImageYear;

            $form->setDefaults([
                "cat_name" => $category->cat_name,
                "description" => $category->description,
                "location" => $category->location,
                "text" => Strings::trim($category->text),
                "year" => $year
            ]);
        }

        return $form;
    }

    public function headerFormProcess(Form $form)
    {
        $update = $this->sessionData->update;
        $values = $form->getValues();
        if (!$update) {
            $category = new Image_category();
            $category->setCatName($values->cat_name);

            if ($values->location != null) $category->setLocation($values->location);

            $category->setDescription($values->description);
            $category->setText($values->text);

            $this->em->persist($category);
            $this->em->flush();
            $category_id = $category->getId();
        } else {
            $category = $this->em->getRepository(Image_category::getClassName())->findOneBy(["id" => $this->sessionData->ImageCategory->id]);
            $category_id = $category->id;

            $category->setCatName($values->cat_name);
            $category->setDescription($values->description);
            $category->setLocation($values->location);
            $category->setText($values->text);

            $this->em->persist($category);

            $this->em->flush();

            $images = $this->em->getRepository(Image::getClassName())->findBy(["category_id" => $category_id]);

            foreach ($images as $image) {
                $image->setYear($values->year);
                $image->setAlt($values->description);
                $this->em->persist($image);
            }

            $this->em->flush();

        }
        $i = 0;
        foreach ($values->image as $value_img) {
            $img = \Nette\Utils\Image::fromFile($value_img);
            $datetime = new DateTime();
            $imgName = md5($value_img->name . time());

			$img->resize(NULL, 644, \Nette\Utils\Image::SHRINK_ONLY);

			$height = $img->getHeight();
			$width = $img->getWidth();
			$quality = 75;

			$image = new Image();
			$image->setName($imgName);
			$image->setAlt($values->description);
			$image->setCategoryId($category_id);
			$image->setUploaded($datetime);
			$image->setHeight($height);
			$image->setWidth($width);
			$image->setQuality($quality);
			$image->setYear($values->year);
			if (!$update) {
				($i == 0) ? $image->setMain(1) : $image->setMain(0);
			} else {
				$image->setMain(0);
			}

			$img->save("www/photos/" . $imgName . "_" . $width . ".jpg", $quality);

			$this->em->persist($image);
			$this->em->flush();

			$i++;
        }
    }

}


interface ImageFormFactory
{
    /**
     * @return ImageForm
     */
    public function create();
}