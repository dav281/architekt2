<?php

namespace App\Admin\Components;

use App\Admin\Model\Entities\Architect;
use Doctrine\ORM\EntityManager;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Tracy\Debugger;

class ArchitectForm extends Control
{
    /** @var EntityManager @inject */
    public $em;

    public function setEntityManager(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function render()
    {
        $template = $this->getTemplate();
        $template->setFile(__DIR__ . "/templates/default.latte");
        $template->render();
    }


    public function createComponentArchitectForm()
    {
        $form = new Form();

        $form->addText("name", "Jméno: ")
            ->setRequired();
        $form->addTextArea("text", "Text: ", 81, 9)
            ->setRequired();
        $form->addUpload("image", "Fotka: ")
            ->addRule(Form::IMAGE, "Lze vložit pouze obrázky")
            ->setRequired();

        $form->addSubmit("submit", "Přidat architekta");

        $form->onSubmit[] = [$this, 'formProcess'];

        return $form;
    }

    public function formProcess(Form $form)
    {
        $values = $form->getValues();

        $architect = new Architect();
        $architect->setName($values->name);
        $architect->setDescription($values->text);

        $image = $values->image;
        $imgName = md5($image->name + time());

        $img = \Nette\Utils\Image::fromFile($values->image);

        $width = 221;
        $height = 221;

        $img->resize($width, $height, \Nette\Utils\Image::EXACT | \Nette\Utils\Image::SHRINK_ONLY);
        $img->save("www/images/architects/" . $imgName . "_" . $width . ".jpg");

        $architect->setImage($imgName);

        $this->em->persist($architect);
        $this->em->flush();

        $this->flashMessage("Architekt byl přidán", "success");
    }

}

interface ArchitectFormFactory
{
    /**
     * @return ArchitectForm
     */
    public function create();
}