<?php

namespace App\Admin\Components;

use App\Admin\Model\UserManager;
use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Tracy\Debugger;

class LoginForm extends Control
{

    /** @var UserManager */
    private $userManager;

    public function setUserManager(UserManager $um){
        $this->userManager = $um;
    }

    public function render()
    {

        $this->template->setFile(__DIR__ . "/templates/@layout.latte");

        $this->template->render();
    }

    /**
     * @return Form
     */
    public function createComponentAdminForm()
    {
        $form = new Form();

        $form->addText("user", "Email:")
            ->setRequired();

        $form->addPassword("password", "Heslo: ")
            ->setRequired();

        $form->addSubmit("submit", "Přihlásit se");

        $form->addProtection();

        $form->onSuccess[] = [$this, "adminProcess"];

        return $form;
    }

    public function adminProcess(Form $form, $values)
    {
        $row = $this->userManager->getCredentials($values->user);

        if (!$row) {
            $this->presenter->flashMessage("Špatně zadané údaje");
            $this->redirect('this');
        } elseif (!Nette\Security\Passwords::verify($values->password, $row->password)) {
            $this->presenter->flashMessage("Špatně zadané údaje");
            $this->redirect('this');
        } else {
            $this->presenter->getUser()->login($values->user, $values->password);

            $this->presenter->redirect("Homepage:");
        }

    }
}

interface LoginFormFactory
{
    /**
     * @return LoginForm
     */
    public function create();
}