<?php

namespace App\Admin\Presenters;

use App\Admin\Components\ArchitectFormFactory;
use App\Admin\Components\LoginFormFactory;
use App\Admin\Components\ImageFormFactory;
use Doctrine\ORM\EntityManager;
use Nette;
use App\Admin\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    public $userManager;

    public $entityManager;

    /** @var \Nette\Http\Session */
    private $session;

    /** @var \Nette\Http\SessionSection */
    private $sessionData;
	

    public function __construct(Model\UserManager $userManager, EntityManager $entityManager, Nette\Http\Session $session)
    {
        $this->userManager = $userManager;
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    /** @var LoginFormFactory @inject */
    public $loginFormFactory;

    /** @var ImageFormFactory @inject */
    public $imageFormFactory;

    /** @var ArchitectFormFactory @inject */
    public $architectFormFactory;

    public function createComponentAdminForm()
    {
        $form = $this->loginFormFactory->create();
        $form->setUserManager($this->userManager);
        return $form;
    }

    public function createComponentImageForm()
    {
        $form = $this->imageFormFactory->create();
        $form->setEntityManager($this->entityManager);
        $form->setSession($this->session);
        return $form;
    }

    public function createComponentArchitectForm()
    {
        $form = $this->architectFormFactory->create();
        $form->setEntityManager($this->entityManager);
        return $form;
    }

    public function beforeRender()
    {
        if (!$this->getUser()->isLoggedIn() and !$this->getUser()->isInRole("Administrator") and !$this->isLinkCurrent("Homepage:SignIn")) {
            $this->redirect("Homepage:SignIn");
        }

        if ($this->getUser()->isLoggedIn() and $this->getUser()->isInRole("Administrator") and $this->isLinkCurrent("Homepage:SignIn")) {
            $this->redirect("Homepage:");
        }

    }

	public function handleLogout()
	{
		$this->getUser()->logout();
		$this->redirect("Homepage:signIn");
	}

}
