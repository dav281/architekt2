<?php

namespace App\Admin\Presenters;

use App\Admin\Forms\ITitlePhotoDescriptionFormFactory;
use App\Admin\Forms\ITitlePhotoFormFactory;

use App\Admin\Forms\TitlePhotoDescriptionForm;
use App\Admin\Model;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Nette;
use Tracy\Debugger;

class TitlePhotoPresenter extends BasePresenter
{
	/** @var EntityRepository */
	private $titlePhotoRepository;

	/** @var ITitlePhotoFormFactory @inject*/
	public $titlePhotoFormFactory;

	/** @var ITitlePhotoDescriptionFormFactory @inject */
	public $titlePhotoDescriptionFormFactory;

	public function __construct(Model\UserManager $userManager, EntityManager $entityManager, Nette\Http\Session $session)
	{
		parent::__construct($userManager, $entityManager, $session);
		$this->titlePhotoRepository = $this->entityManager->getRepository(Model\Entities\TitlePhoto::class);
	}

	public function renderDefault()
	{
		$this->getTemplate()->titlePhotos = $this->titlePhotoRepository->findBy([], ["ordered" => "ASC"]);
	}

	public function createComponentTitlePhotoForm()
	{
		$form = $this->titlePhotoFormFactory->create($this->entityManager);
		return $form->createComponentForm();
	}

	public function createComponentTitlePhotoDescriptionForm()
	{
		return new Nette\Application\UI\Multiplier(function ($itemId){
			/** @var Model\Entities\TitlePhoto $titlePhoto */
			$titlePhoto = $this->titlePhotoRepository->findOneBy(['id' => $itemId]);
			return $this->titlePhotoDescriptionFormFactory->create($titlePhoto);
		});
	}

	public function handleChangePhotoOrder($items = [])
	{
		foreach ($items as $order => $id)
		{
			$titlePhoto = $this->titlePhotoRepository->findOneBy(['id' => $id]);
			$titlePhoto->setOrdered($order);
			$this->entityManager->persist($titlePhoto);
			$this->entityManager->flush();
		}
	}

	public function handleDeleteTitlePhoto($id)
	{
		$titlePhoto = $this->entityManager->getRepository(Model\Entities\TitlePhoto::class)->findOneBy(['id' => $id]);
		$this->entityManager->remove($titlePhoto);
		$this->entityManager->flush();
		$this->redirect('this');
	}

}