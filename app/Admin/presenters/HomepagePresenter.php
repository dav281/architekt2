<?php

namespace App\Admin\Presenters;

use App\Admin\Model;
use App\Model\ImageManager;
use Doctrine\ORM\EntityManager;
use Nette;
use Tracy\Debugger;


class HomepagePresenter extends BasePresenter
{

    public $imageManager;

    public $architectManager;

    public function beforeRender()
    {
        parent::beforeRender();
    }

    public function __construct(Model\UserManager $userManager, EntityManager $entityManager, Nette\Http\Session $session, ImageManager $imageManager, Model\Entities\ArchitectManager $architect)
    {
        parent::__construct($userManager, $entityManager, $session);
        $this->imageManager = $imageManager;
        $this->architectManager = $architect;
    }

    public function renderDefault()
    {
        $template = $this->getTemplate();
        $template->image_info = $this->imageManager->getImageInfo();
        $images = $this->imageManager->getImagesForRender();
        $sizes = $this->getImageSizes();

        $template->images = $images;
        $template->sizes = $sizes;
    }

    public function renderArchitects()
    {
        $this->getTemplate()->architects = $this->architectManager->getArchitects();
    }

    public function renderDetail($category_id)
    {


        $images = $this->imageManager->getLinkedImages($category_id);
        if ($images == [] || $images == null || empty($images)) {
            $this->redirect("Homepage:");
        }
        $this->getTemplate()->images = $images;
        $this->getTemplate()->category_id = $category_id;
    }

    public function renderSignIn()
    {

    }

    public function createComponentArchitectUpdateForm()
    {
        return new Nette\Application\UI\Multiplier(function ($architect_id) {
            $form = new Nette\Application\UI\Form();
            $architect = $this->architectManager->getArchitect($architect_id);

            $form->addText("name", "Jméno: ")
                ->setRequired();
            $form->addTextArea("text", "Text: ", 81, 9)
                ->setRequired();
            $form->addUpload("image", "Fotka: ")
                ->addRule(Nette\Application\UI\Form::IMAGE, "Lze vložit pouze obrázky")
                ->setRequired(false);

            $form->addSubmit("submit", "Upravit architekta");

            $form->addHidden('architect_id', $architect_id);

            $form->setDefaults([
                "name" => $architect->name,
                "text" => $architect->description
            ]);

            $form->onSubmit[] = [$this, 'formProcess'];

            return $form;
        });
    }

    public function formProcess(Nette\Application\UI\Form $form)
    {
        $values = $form->getValues();
        $architect = $this->entityManager->getRepository(Model\Entities\Architect::getClassName())->findOneBy(["id" => $values->architect_id]);

        $architect->setName($values->name);
        $architect->setDescription($values->text);

        if ($values->image->error == 0) {
            $image = $values->image;
            $imgName = md5($architect->image . time());

            $dir = "www/images/architects";

            foreach (Nette\Utils\Finder::findFiles($architect->getImage() . "*.jpg")->in($dir) as $key => $file) {
                Nette\Utils\FileSystem::delete($key);
            }

            $img = \Nette\Utils\Image::fromFile($image);

            $width = 221;
            $height = 221;

            $img->resize($width, $height, \Nette\Utils\Image::EXACT | \Nette\Utils\Image::SHRINK_ONLY);
            $img->save("www/images/architects/" . $imgName . "_" . $width . ".jpg");

            $architect->setImage($imgName);
        }

        $this->entityManager->persist($architect);
        $this->entityManager->flush();
    }

    public function getImageSizes()
    {
        $sizes = $this->getConfigSizes();

        return $this->imageManager->getImageSizes($sizes);
    }

    public function getConfigSizes()
    {
        $params = $this->context->getParameters();
        $sizes = $params["responsive"]["display"];

        return $sizes;
    }

    public function handleChangeMainImage($id)
    {
        if($this->isAdministrator()) {
            $this->imageManager->changeMainImage($id);
            $this->redirect('this');
        }
    }

    public function handleDeleteImage($id)
    {
        if ($this->isAdministrator()) {
            $this->imageManager->deleteImage($id);
        }
    }

    public function handleDeleteArchitect($id)
    {
        if ($this->isAdministrator()) {
            $architect = $this->entityManager->getRepository(Model\Entities\Architect::getClassName())->findOneBy(["id" => $id]);
            $dir = "www/images/architects";

            foreach (Nette\Utils\Finder::findFiles($architect->getImage() . "*.jpg")->in($dir) as $key => $file) {
                Nette\Utils\FileSystem::delete($key);
            }

            $this->architectManager->deleteArchitect($architect);

            $this->flashMessage("Architekt vymazán", "success");
        }

    }

    public function handleDeleteReference($category_id)
    {
        if ($this->isAdministrator()) {
            $this->imageManager->deleteReference($category_id);
            $this->flashMessage("Reference smazána", "success");
            $this->redirect('this');
        }
    }


    public function isAdministrator()
    {
        if ($this->getUser()->isLoggedIn() and $this->getUser()->isInRole("Administrator")) {
            return true;
        } else {
            return false;
        }
    }

}
