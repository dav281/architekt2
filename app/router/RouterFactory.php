<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
        $router[] = new Route('administrace/prihlaseni', "Admin:Homepage:signIn");
        $router[] = new Route('administrace/architekti', "Admin:Homepage:architects");
        $router[] = new Route('administrace/titulni-fotky', "Admin:TitlePhoto:default");
        $router[] = new Route('administrace', "Admin:Homepage:default");
		$router[] = new Route('<presenter>/<action>', 'Web:Homepage:default');
 		return $router;
	}

}
